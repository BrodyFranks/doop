#define MULTIPLE_SEGREGATED_ANALYSES

// For this analysis, every heap context recorded on allocation
// corresponds to the most significant element of the calling
// context of the allocator method.
#define RecordMacro(ctx, value, hctx) \
  ctx = [__partition, __ctxVal1, __ctxVal], \
  hctx = [__partition, __ctxVal]

#define AdvancedRecordBasisMacro(ctx, var, value) \
  RecordBasis(var, ctx, value)

#define CachedRecordMacro(ctx, value, hctx) \
  CachedRecord(ctx, value, hctx)

#define CompatibleContextAndHContextMacro(ctx, hctx) \
  ctx = [__partition, __ctxv1, __ctxv2], \
  hctx = [__partition, __hctxv]

#define CompatibleHContextsMacro(hctx1, hctx2) \
  hctx1 = [__partition, __hctxv1], \
  hctx2 = [__partition, __hctxv2]

// For this analysis the context of a method call corresponds to the
// identity of the receiver object and the receiver object of the caller.
// Again, this may trigger creation of a new object.
#define MergeMacro(callerCtx, invocation, hctx, value, calleeCtx) \
  hctx = [__partition, __value1], \
  calleeCtx = [__partition, __value1, value]

#define AdvancedMergeBasisMacro(callerCtx, invocation, hctx, value) \
  MergeBasis(callerCtx, invocation, hctx, value)

#define CachedMergeMacro(callerCtx, invocation, hctx, value, calleeCtx) \
  CachedMerge(invocation, hctx, value, calleeCtx)

// For this analysis, static calls just keep the same context as the
// caller.
#define MergeStaticMacro(callerCtx, invocation, calleeCtx) \
  callerCtx = calleeCtx

#define AdvancedMergeStaticBasisMacro(callerCtx, invocation) \
  MergeStaticBasis(callerCtx, invocation)

#define CachedMergeStaticMacro(callerCtx, invocation, calleeCtx) \
  CachedMergeStatic(callerCtx, invocation, calleeCtx)

// MergeThreadStart, MergeStartup, and MergeFinalizerRegisterContext
// have the same logic as plain Merge for this analysis.
#define MergeThreadStartMacro(hctx, value, callerCtx, newCtx) \
  hctx = [__partition, __value1], \
  newCtx = [__partition, __value1, value]

#define MergeStartupMacro(hctx, value, calleeCtx) \
  hctx = [__partition, __value1], \
  calleeCtx = [__partition, __value1, value]

#define MergeOpenProgramEntryPointMacro(method, value, calleeCtx) \
  isContext(calleeCtx), \
  PartitionForValue(value, __partition), \
  calleeCtx = [__partition, value, value]

// This is exactly equivalent to the regular merge logic, but written
// differently. At finalization, we create a new hctx, based on the
// callerCtx, and then use this new hctx as we would in regular Merge.
// The macro below does this, without referring to the new hctx (i.e.,
// using knowledge of how it is created). This is necessary because since
// the new hctx is created in the same rule, it will not yet have values
// for its inverse functions (RealHContextFromHContext), so the rule will never
// fire if done naively. The signature of the macro (which does not accept a
// hctx) is a hint for avoiding this problem.
#define MergeFinalizerRegisterContextMacro(callerCtx, inmethod, value, newCtx) \
  callerCtx = [__partition, __ctxval, __value1], \
  newCtx = [__partition, __value1, value]

#define InitContextMacro(value) \
  isContext(ctx) :- \
  ctx = [__partitionId, value, value] , \
  isPartition(__partitionId)

#define InitHContextMacro(value) \
  isHContext(hctx) :- \
  hctx = [__partitionId, value], \
  isPartition(__partitionId)
